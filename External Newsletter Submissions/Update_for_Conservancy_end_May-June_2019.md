Inkscape Update for Conservancy newsletter June 2019 



The Great Inkscape Bug Migration began in May 2019 and is ongoing. This gamified migration strategy aims to help the project consolidate its bugs onto the GitLab platform, where the rest of the project's repository his housed. Several members of the Inkscape project were present and active at the Saarbrücken Hackfest and LGM Meeting 2019. Thanks to K8 and @Houz for the warm welcome & the digs. Highlights included running out of stickers, making new friends and achieving some major goals for the upcoming 1.0 release: solid progress on the macOS integration, a unified trace bitmap dialogue and a second 1.0alpha release. Inkscapers also presented at LGM on new features to watch for along with demos of new Live Path Effects (LPEs). [Watch Inkscape's web site](www.inkscape.org) and social media platforms for news and photos. 







