# Inkscape Vectors > Content

This project is the official place for content writers, designers, marketers and other profiles to contribute to creating new content for the Inkscape website and social media. It covers all stages of the publishing process, from proposing an idea, editing text and media files, scheduling and tracking of the publication in different media.

This is a marketing effort around Inkscape project. Contributing to the Inkscape app is being done in separate projects here on GitLab.  

## Getting Started

You can:

* jump right into the [Issues](https://gitlab.com/inkscape/vectors/content/issues) list and see if you can help with any of the titles
* or [propose](https://gitlab.com/inkscape/vectors/content/issues/new) a new article/video/tutorial/document/media release.

Issues are marked with labels such as ~Writing, ~Design and ~Video. You can use them to filter the list and see only the ones you're interested in.

If you wish to discuss things first, introduce yourself, or need any help or feedback:

* join the Vectors Team Chat channel [#team_vectors](https://chat.inkscape.org/channel/team_vectors).
* More details: <a href="https://gitlab.com/inkscape/vectors/content/wikis/home">Tutorial—How to use GitLab to contribute to Inkscape marketing</a>

### Prerequisites

You're not unfamiliar with the concept of open source projects and you have an inclination to any of the following:

* Writing news articles (in English or other languages)
* Designing wireframes and mockups
* Creating and processing videos
* Documenting features
* Writing tutorials
* Using social media platforms
* Translating text into other languages
* Public relations
* Organizing events
* Community management
* any other similar activity

You don't have to:

* Have the latest Inkscape version (or any!)
* Know how to programme
* Know how to use GitLab (but a learning attitude is helpful)

## How is all this organized?

There are two important places where stuff happens: **Issues** and **Repository**.

### Issues

Issues is a task tracker tool where each article/media item has a dedicated entry. This is where the discussion is happening and publishing process is tracked through subtasks. We usually discuss wording and proof-check facts.  

Media items' Issues are connected to the relevant article Issue through the `Related` feature.

### Repository

Repository is a group of folders and files on GitLab where we track changes to articles text and other accompanying media files.

Files can be edited through GitLab interface or the CLI way through Git and cloning the repository to your own computer.

Every commited change is linked to the appropriate Issue by mentioning the Issue ID number in the commit message `#ID`. You can change more than one file and commit the changes together if they are related to the same Issue.

### Editing files

You can use GitLab interface to modify text files directly through your browser. It isn't necessary to have Git installed on your computer.

GitLab interface allows you to:

* see folders and files structure
* see files revision history
* compare file revision changes
* see editors' comments
* upload new files
* commit changes to files

Media files will have to be downloaded to your own computer where you edit them, and then upload the modified version.

> The commit message should reflect your intention, not the contents of the commit. The contents of the commit can be easily seen anyway, the question is why you did it.

Read the full tutorial: 

**<a href="https://gitlab.com/inkscape/vectors/content/wikis/home">Tutorial—How to use GitLab to contribute to Inkscape marketing</a>**

### Git clone

If you're familiar with Git you can clone this repository to your own computer the usual way.

This way you can edit the files using your favorite editors, commit changes locally, and then push the changes to GitLab.

```sh
git clone git@gitlab.com:inkscape/vectors/content.git
cd content
git pull
# Edit a text file
nano article.md
# Edit an image through Inkscape, GIMP, etc
{outside application}
# Stage the changes
git add article.md article-image.png
# Check status 
git status
# Commit the staged changes explaining the goal of the change and adding
# the Issue ID number
git commit -m "Fixed typos in article text and accompanying banner #34"
git pull
git push
```

## License

All work contributed to this project should be licensed under the [LICENSE](LICENSE).  

## Acknowledgments

* Hat tip to anyone whose code, words, translations or design skills were used
* Special thanks to @moini and @zigzagmlt for their continued relentless support

