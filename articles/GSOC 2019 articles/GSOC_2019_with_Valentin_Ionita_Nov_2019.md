(GSOC 2019 with @vanntile) 



Valentin Ionita wrangled meshes, hatches and GTK during a summer with Inkscape in 2019 

When the Inkscape community seeks Google Summer of Code (GSoC) applicants, we're hoping to find people who are curious about the project and the software. Part of the application process requires applicants to complete sample work on the project to demonstrate their skills and understanding.

In the winter of 2019, Valentin Ionita (@vanntile) joined the community, tackled several tasks and shared the outline of a project he wanted to work on as a GSoC student. 

"The GSoC experience was something I had been waiting for a whole year, since the first time I ~~have~~ had heard of a program like this. ~~Moreover,~~ ~~w~~  **W**hen I found Inkscape to be one of the organizations, a program I had been using for such a long time, I decided to be one of the mentees," said Ionita, who hails from Bucharest, Romania.

By the summer our GSoC student not only knew the program, but got involved in our online community. Ionita brought his curiosity, determination and coding skills to the project. Top Inkscape contributor Tavmjong Bah (@Tav) mentored Ionita during his work.

Ionita proposed developing JavaScript polyfills to increase their functionality within Inkscape and improve image rendering by other paint servers in browsers. While it began there, Iona's contribution evolved to working on mesh gradients and hatches to enable non-technical users of Inkscape to more easily use them. 

His work on mesh gradient polyfills aimed to improve the code quality by switching to "modern JavaScript syntax without losing performance [and] increasing performance, if possible." The next part of this work had Ionita digging back into theories in mathematical textbooks and Wikipedia to solve the problem.

 

![pepper_rendered] from @vanntile's document here: https://gitlab.com/vanntile/inkscape-gsoc-application  



Caption: ??



"Hatches," he explained, "are specialised paint servers that repeat one or several `hatchpaths` by a fixed formula based on the element's attributes - x, y, rotate and pitch." 

Iona worked on improving the UI and UX of hatches in the Inkscape interface. This meant "editing handles for hatches that appear when you have an element with a hatch and use a specific set of tools, for example, the node-editing tool. Using the handles, you ~~could~~ **can** rotate, scale and translate the paint server."

While he experienced a steep learning curve with managing GNOME widgets, in the end, "the best source of understanding how a complex behavior using GTK widgets was actually Inkscape." 

Working on "improving the actual Inkscape interface and functionality concerning paint servers. This was completely different for me, passing from a scripting language, JavaScript, to a programming one, C++, and, especially, the GTK framework to manage the interface," Iona explained. 

Then Ionita worked on improving the support of hatches in Inkscape. "My work tried to add small UI elements available to other paint servers (i.e. patterns) to the hatches and introduce a new dialog in Inkscape with the purpose of previewing and setting the paint server." This work will serve in the development of a new tool in the program to manage, preview and set all types of paint servers from resource files within Inkscape and from users. 

Inkscape definitely gained much through Ionita's contribution, which continues to this day. Understanding how contributing to Inkscape helped him is also part of our learning as a community. 

Among the takeaways for Ionita, include code-specific experiences and learning, he learned that "patience is required when exploring solving a problem; on a decentralized project, involvement in secondary features is the duty of the independent developers, not the main team, and asking the community can be the greatest source of truth."

Finally, he said, "I appreciate everyone from the Inkscape team, ~~be them~~ whether they are in the dev, ux or the vectors teams. Thank you!"

We thank Valentin Ionita for his ongoing contribution to this project. We're excited to see what he contributes next. 

Check out Ionita's full-length documentation ( https://gitlab.com/vanntile/inkscape-gsoc-application ) on the work he accomplished during his GSoC in 2019.

If you're interested in becoming an Inkscape contributor, head here ( https://inkscape.org/contribute/ ) and then consider joining our community ( https://inkscape.org/community/ ). 