# Inkscape Vectors Onboarding Survey

## Content
### Landing Page

Hello! Welcome to the Inkscape project! Thank you for your interest in the [Vectors team](https://inkscape.org/*inkscape-vectors/)! We are always happy for new teammates to join us in broadening adoption, increasing contributions, nurturing partnerships, and deepening loyalty of Inkscape users. Artists, designers, developers, marketers, students, and anybody else interested are welcome to join the effort! Check out our [issues on GitLab](https://gitlab.com/groups/inkscape/vectors/-/issues), if you are curious about what we work on! 

This survey is for us to understand how you wish to contribute to Inkscape as part of the Vectors team, so that we can better help you get started :)

## Questions

1. I am an … 
    * Artist 
    * Graphic Designer 
    * Writer
    * Programmer 
    * Teacher 
    * Maker
    * Scientist 
    * Marketer
    * Project Manager
    * Free/Libre and Open Source Software (F/LOSS) fan 
1. I think Inkscape teams should focus more on:  
1. I would like to contribute to Inkscape as part of the Vectors team by … 
    * Writing articles 
    * Writing tutorials
    * Managing social media communications 
    * Creating posters, illustrations and other marketing collateral 
    * Revising Inkscape app UX and UI 
    * Creating and processing videos 
    * Organizing events
    * Hosting podcasts (you would love [Inkscope](https://www.youtube.com/watch?v=1J1r8a0emYc)!)
    * Programming (CSS)
    * Programming (Django) 
1. I found out about the Vectors team through …
1. You'll recognize me under this alias (on Inkscape chat or GitLab): 


### End Page

On behalf of the Inkscape project, contributors and users, thank you your time and interest! We appreciate your time and interest in contributing to Inkscape, and the Vectors team in particular. We'll get in touch with you as soon as we can. In the meantime, you're welcome to join our chat channel here [#team_vectors](https://chat.inkscape.org/channel/team_vectors).
