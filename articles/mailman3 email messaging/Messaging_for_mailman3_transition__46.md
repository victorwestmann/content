* [ ] Subject: [ANNOUNCE] This mailing list is moving soon!

> The Inkscape project is upgrading its mailing lists, and will be moving off SourceForge.  You don't need to do anything yet, but on ${DATE} you will need to subscribe to the new list (SF doesn't provide subscriber lists).
>
> If you wish to pre-subscribe to the new mailing lists to ensure you don't miss any Inkscape discussions, please visit <https://lists.inkscape.org>!
>
> 

* [ ] Subject: [ANNOUNCE] This mailing list is moving soon!
> The Inkscape project is upgrading its mailing lists, and will be moving off SourceForge.  You don't need to do anything yet, but on ${DATE} you will need to subscribe to the new mailing list (SF doesn't provide subscriber lists any more).
>
> If you wish to pre-subscribe to the new lists to ensure you don't miss any Inkscape discussions, please visit https://lists.inkscape.org!


* [ ] Subject: [IMPORTANT] This mailing list has moved to lists.inkscape.org!
> This list has moved - you must resubscribe at https://lists.inkscape.org to continue receiving email.
>
> Inkscape is transitioning away from SourceForge, and upgrading to a newer mailing list management service and a better email archive system.  We cannot access the subscriber list, so cannot just transfer your subscription to the new system.

* [x] Subject: [ANNOUNCE] This mailing list will be closing soon

> Inkscape is migrating mail service off SourceForge. Since the traffic on this mailing list has been so light the last few years, we are not planning on continuing this list in our new hosting plans.
>
> However, if you feel strongly that this mailing list *is* still needed, please just let us know by replying to this message. If we see at least 3 people who are interested in continuing the mailing list on the new systems, we can include it in the migration. Let us know before ${DATE}.
> 

* [ ] Subject: [IMPORTANT] This mailing list has closed

> Inkscape is migrating mail service off SourceForge. Since the traffic on this mailing list has been so light the last few years, we have decided to bring it to conclusion.
>
> To continue participating in Inkscape discussions, please visit <https://lists.inkscape.org/> and subscribe to mailing lists that match your interests.